package itera;

import java.util.Iterator;

public class ParcoursLigne extends Parcours{
	
	

	public ParcoursLigne(TableauEntier ta) {
		super(ta);
	}


	
	public void suivant() {
		int i = this.nbparcourus;
		if(this.colonneCour + 1 ==this.t.getLargeur()) {
			if(this.ligneCour + 1 != this.t.getHauteur()) {
				this.colonneCour=0;
				this.ligneCour+=1;
			}
		}
		else {
			this.colonneCour+=1;
		}
		this.nbparcourus+=1;
	}

	

}
