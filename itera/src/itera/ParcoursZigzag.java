package itera;

public class ParcoursZigzag extends Parcours{


	
	
	public ParcoursZigzag(TableauEntier ta) {
		super(ta);
		// TODO Auto-generated constructor stub
	}



	@Override
	public void suivant() {
		if(super.hasNext()) {
			int diviseur = (int) this.ligneCour/2;
			if(this.ligneCour== (2*diviseur)+1) { //impair --->
				int i = this.nbparcourus;
				if(this.colonneCour + 1 ==this.t.getLargeur()) {
					if(this.ligneCour + 1 != this.t.getHauteur()) {
						this.colonneCour=0;
						this.ligneCour+=1;
					}
				}else {
					this.colonneCour+=1;
				}
			}else { //pair <---
				int i = this.nbparcourus;
				if(this.colonneCour  ==0) {
					if(this.ligneCour + 1 != this.t.getHauteur()) {
						this.ligneCour+=1;
					}
				}else {
					this.colonneCour-=1;
				}
			}
		}
	}

}
