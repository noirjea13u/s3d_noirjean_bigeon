package itera;

import java.util.Iterator;

public abstract class Parcours implements Iterator<Integer>{
	
	protected int ligneCour;;
	protected  int colonneCour;
	protected  int nbparcourus;
	protected  TableauEntier t;
	
	public Parcours(TableauEntier ta) {
		t = ta;
	}

	public Integer next() {
		int vak = t.valeurA(ligneCour, colonneCour);
		suivant();
		nbparcourus ++;
		return vak;
	}
	
	public abstract  void suivant();
	
	public boolean hasNext() {
		return (nbparcourus < t.getLargeur() * t.getHauteur());
	}
	
	
	
}
