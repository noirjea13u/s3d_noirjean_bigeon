package itera;

import java.util.Iterator;

public class TableauEntier {
	
	private int[][] tab;

	public TableauEntier(int[][] t) {
		tab = t;
	}
	
	public int getLargeur() {
		return tab[0].length;
	}

	public int getHauteur() {
		return tab.length;
	}
	
	public int valeurA(int l, int c) {
		return tab[c][l];
	}
	
	public ParcoursLigne iterateurLigne() {
		return (new ParcoursLigne(this));
	}
	
	public ParcoursZigzag iterateurZigzag() {
		return (new ParcoursZigzag(this));
	}
	

	public static void main( String[] args) {
		
		int[][] tt ={ {1,2},{3,4}};
		TableauEntier te = new TableauEntier(tt);
		Iterator<Integer> it;
		
		it = te.iterateurLigne(); 
		
	}
}
